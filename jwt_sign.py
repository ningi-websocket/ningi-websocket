import json
import pathlib

import jwt

# pip install pyjwt

ALGO_KEY = {
    "ES256": "es256.pem",
    "ES384": "es384.pem",
    "RS256": "rsa_aes.pem",
    "RS384": "rsa_aes.pem",
    "RS512": "rsa_aes.pem",
    "EdDSA": "eddsa.pem",
}

ALGO_OUT = {
    "ES256": "es256",
    "ES384": "es384",
    "RS256": "rs256",
    "RS384": "rs384",
    "RS512": "rs512",
    "EdDSA": "eddsa",
}


def encode_jwt(data: dict, algo: str):
    _key = ALGO_KEY[algo]
    with open(f"var/pem/{_key}", "r") as r:
        content = r.read()
    key = jwt.encode(data, key=content, algorithm=algo)
    return key


def make_jwt(files: list, _input: str, _output: str, algo: str):
    _key = ALGO_OUT[algo]
    for f in files:
        with open(f"{_input}/{f}.json", "r") as r:
            _encoded_key = encode_jwt(json.loads(r.read()), algo)
            _out = f"{_output}/{_key}"
            pathlib.Path(_out).mkdir(exist_ok=True)
            with open(f"{_out}/{f}.txt", "w") as w:
                w.write(_encoded_key)


if __name__ == '__main__':
    _files = [
        "expired",
        "normal",
        "with_nbf",
        "with_nbf_early",
    ]

    _input = "var/jwt/jwt_inputs"
    _output = "var/jwt"

    for algo in ALGO_KEY.keys():
        try:
            make_jwt(_files, _input, _output, algo)
        except TypeError as e:
            print(f"{algo}: {e}")
