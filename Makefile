.PHONY: all
all: install-rust release

.PHONY: install-rust
install-rust:
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup.sh && chmod +x rustup.sh && ./rustup.sh -y && rm rustup.sh

.PHONY: update-rust
update-rust:
	rustup update

.PHONY: release
release:
	cargo build --release
	upx-4.2.3-amd64_linux/upx --best --lzma target/release/ningi_ws

.PHONY: build
build:
	cargo build

.PHONY: run
run: build
	RUST_LOG=debug target/debug/ningi_ws

.PHONY: gen-cert
gen-cert:
	openssl genrsa -aes256 -passout file:var/.password.txt -out var/pem/rsa_aes.key 2048
	openssl rsa -in var/pem/rsa_aes.key -passin file:var/.password.txt -outform PEM -pubout -out var/pem/rsa_aes.pem.pub
	openssl rsa -in var/pem/rsa_aes.key -passin file:var/.password.txt -out var/pem/rsa_aes.pem

	openssl ecparam -name prime256v1 -genkey -noout -out var/pem/es256.pem
	openssl ec -in var/pem/es256.pem -pubout -out var/pem/es256.pem.pub

	openssl ecparam -name secp384r1 -genkey -noout -out var/pem/es384.pem
	openssl ec -in var/pem/es384.pem -pubout -out var/pem/es384.pem.pub

	openssl genpkey -algorithm ed25519 -out var/pem/eddsa.pem
	openssl pkey -in var/pem/eddsa.pem -pubout -out var/pem/eddsa.pem.pub
