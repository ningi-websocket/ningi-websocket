use uuid::Uuid;
use warp::{Filter, Rejection, Reply};

use crate::app_jwt::AppJwt;
use crate::broadcast_client::BroadcastClients;
use crate::config::AppConfig;
use crate::handlers::*;
use crate::websocket::client::ws_client::WsClients;

async fn route_root(
    clients: WsClients,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!()
        .and(warp::get())
        .and(warp::any().map(move || clients.clone()))
        .and_then(root_handler)
}

async fn route_register(
    cfg: AppConfig,
    clients: WsClients,
    app_jwt: AppJwt,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("register")
        .and(warp::post())
        .and(warp::body::content_length_limit(4096))
        .and(warp::body::json())
        .and(warp::any().map(move || cfg.clone()))
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || app_jwt.clone()))
        .and_then(register_handler)
}

async fn route_ws(
    cfg: AppConfig,
    clients: WsClients,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("ws" / Uuid)
        .and(warp::ws())
        .and(warp::any().map(move || cfg.clone()))
        .and(warp::any().map(move || clients.clone()))
        .and_then(ws_handler)
}

async fn route_broadcast(
    cfg: AppConfig,
    clients: WsClients,
    broadcast_clients: BroadcastClients,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("ws" / "broadcast")
        .and(warp::ws())
        .and(warp::any().map(move || cfg.clone()))
        .and(warp::any().map(move || clients.clone()))
        .and(warp::any().map(move || broadcast_clients.clone()))
        .and_then(ws_broadcast_handler)
}

async fn route_publish(
    cfg: AppConfig,
    clients: WsClients,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    warp::path!("publish")
        .and(warp::post())
        .and(warp::body::json())
        .and(warp::any().map(move || cfg.clone()))
        .and(warp::any().map(move || clients.clone()))
        .and_then(publish_handler)
}

pub async fn routes(
    cfg: AppConfig,
    clients: WsClients,
    app_jwt: AppJwt,
    broadcast_clients: BroadcastClients,
) -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    let real_ip_header = cfg.daemon.ip_header.clone();

    let log = warp::log::custom(move |info| {
        let remote_ip = match info.remote_addr() {
            Some(ip) => ip.ip().to_string(),
            _ => "unknown".to_string(),
        };
        let ip = match real_ip_header.clone() {
            Some(k) => match info.request_headers().get(k.as_str()).map(|v| v.to_str()) {
                Some(o) => o.unwrap_or("unknown").to_string(),
                _ => remote_ip,
            },
            _ => remote_ip,
        };
        log::debug!(
            r#"REQ: {} {} {} {} {:?} "{}""#,
            info.method(),
            info.path(),
            ip,
            info.status(),
            info.elapsed(),
            info.user_agent().unwrap_or("unknown"),
        )
    });

    route_root(clients.clone())
        .await
        .or(route_register(cfg.clone(), clients.clone(), app_jwt.clone()).await)
        .or(route_ws(cfg.clone(), clients.clone()).await)
        .or(route_broadcast(cfg.clone(), clients.clone(), broadcast_clients).await)
        .or(route_publish(cfg.clone(), clients.clone()).await)
        .with(log)
}
