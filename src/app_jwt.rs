use std::collections::HashSet;
use std::fs::File;
use std::io::Read;
use std::path::Path;

use jsonwebtoken::errors::Error;
use jsonwebtoken::{decode, Algorithm, DecodingKey, TokenData, Validation};
use serde::Deserialize;

use crate::requests::Claims;

#[derive(Clone)]
pub struct AppJwt {
    key: DecodingKey,
    validation: Validation,
}

#[derive(Deserialize, Clone)]
pub enum JwtAlgo {
    /// ECDSA using SHA-256
    ES256,
    /// ECDSA using SHA-384
    ES384,
    /// RSASSA-PKCS1-v1_5 using SHA-256
    RS256,
    /// RSASSA-PKCS1-v1_5 using SHA-384
    RS384,
    /// RSASSA-PKCS1-v1_5 using SHA-512
    RS512,
    /// Edwards-curve Digital Signature Algorithm
    EdDSA,
}

impl JwtAlgo {
    fn get_validation(&self) -> Validation {
        let mut validation = match self {
            JwtAlgo::ES256 => Validation::new(Algorithm::ES256),
            JwtAlgo::ES384 => Validation::new(Algorithm::ES384),
            JwtAlgo::RS256 => Validation::new(Algorithm::RS256),
            JwtAlgo::RS384 => Validation::new(Algorithm::RS384),
            JwtAlgo::RS512 => Validation::new(Algorithm::RS512),
            JwtAlgo::EdDSA => Validation::new(Algorithm::EdDSA),
        };
        let mut claims = HashSet::with_capacity(2);
        claims.insert("exp".to_owned());
        claims.insert("userId".to_owned());
        validation.required_spec_claims = claims;
        validation
    }
}

impl AppJwt {
    pub fn new(path: &Path, algo: JwtAlgo) -> Result<AppJwt, String> {
        let content = match get_key_content(path) {
            Ok(o) => o,
            Err(e) => return Err(e),
        };
        let key = match load_key(content, algo.clone()) {
            Ok(o) => o,
            Err(e) => return Err(format!("Cannot load jwt key: {e}")),
        };

        Ok(Self {
            key,
            validation: algo.get_validation(),
        })
    }

    pub fn decode(&self, token: String) -> Result<TokenData<Claims>, Error> {
        decode(&*token, &self.key, &self.validation)
    }
}

#[inline]
fn get_key_content(path: &Path) -> Result<Vec<u8>, String> {
    let mut file = match File::open(path) {
        Ok(o) => o,
        Err(e) => return Err(format!("Cannot open jwt key file: {e}")),
    };
    let mut content = vec![];
    if let Err(e) = file.read_to_end(&mut content) {
        return Err(format!("Cannot read jwt key file: {e}"));
    };
    Ok(content)
}

#[inline]
fn load_key(content: Vec<u8>, algo: JwtAlgo) -> jsonwebtoken::errors::Result<DecodingKey> {
    match algo {
        JwtAlgo::ES256 | JwtAlgo::ES384 => DecodingKey::from_ec_pem(content.as_slice()),
        JwtAlgo::RS256 | JwtAlgo::RS384 | JwtAlgo::RS512 => {
            DecodingKey::from_rsa_pem(content.as_slice())
        }
        JwtAlgo::EdDSA => DecodingKey::from_ed_pem(content.as_slice()),
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;
    use std::str::FromStr;

    use uuid::Uuid;

    use crate::app_jwt::{get_key_content, AppJwt, JwtAlgo};

    fn _tokens_() -> Vec<&'static str> {
        vec![
            "expired.txt",
            "normal.txt",
            "with_nbf.txt",
            "with_nbf_early.txt",
        ]
    }

    fn _token_content(token: &str) -> String {
        let content = get_key_content(Path::new(token)).unwrap();
        String::from_utf8(content).unwrap()
    }

    fn _test_jwt_(pub_key_name: &str, token_name: &str, token_dir: &str, algo: JwtAlgo) {
        match AppJwt::new(Path::new("var/pem").join(pub_key_name).as_path(), algo)
            .unwrap()
            .decode(_token_content(
                format!("var/jwt/{token_dir}/{token_name}").as_str(),
            )) {
            Ok(_) if token_name == "expired.txt" => {
                panic!("{token_dir}/{token_name}: Unexpected expired success")
            }
            Err(_) if token_name == "expired.txt" => {}
            Ok(t) => {
                let id = Uuid::from_str("10a7b36c-3408-40c0-aea7-5077db67d936").unwrap();
                assert_eq!(id, t.claims.user_id);
                assert!(t.claims.exp > 1718410165, "Bad exp: {}", t.claims.exp,);
            }
            Err(e) => {
                panic!("{token_dir}/{token_name}: {e}")
            }
        }
    }

    #[test]
    fn test_content_txt() {
        let result = get_key_content(Path::new("var/test_file.txt"));
        assert!(result.is_ok());
        assert_eq!("ŢēşŧċȏͶτΈϰ¶°".as_bytes(), result.unwrap().as_slice())
    }

    #[test]
    fn test_content_bin() {
        let result = get_key_content(Path::new("var/test_file.bin"));
        assert!(result.is_ok());
        assert_eq!(
            vec![
                0x4F, 0x32, 0x1D, 0x31, 0xD7, 0x4A, 0x28, 0x85, 0xB8, 0x7F, 0x8B, 0xED, 0xDD, 0xA8,
                0xD8, 0x51
            ],
            result.unwrap()
        )
    }

    #[test]
    fn test_rsa_aes256() {
        for t in _tokens_() {
            _test_jwt_("rsa_aes.pem.pub", t, "rs256", JwtAlgo::RS256)
        }
    }

    #[test]
    fn test_rsa_aes384() {
        for t in _tokens_() {
            _test_jwt_("rsa_aes.pem.pub", t, "rs384", JwtAlgo::RS384)
        }
    }

    #[test]
    fn test_rsa_aes512() {
        for t in _tokens_() {
            _test_jwt_("rsa_aes.pem.pub", t, "rs512", JwtAlgo::RS512)
        }
    }

    #[test]
    fn test_es256() {
        for t in _tokens_() {
            _test_jwt_("es256.pem.pub", t, "es256", JwtAlgo::ES256)
        }
    }

    #[test]
    fn test_es384() {
        for t in _tokens_() {
            _test_jwt_("es384.pem.pub", t, "es384", JwtAlgo::ES384)
        }
    }

    #[test]
    fn test_ed255() {
        for t in _tokens_() {
            _test_jwt_("eddsa.pem.pub", t, "eddsa", JwtAlgo::EdDSA)
        }
    }
}
