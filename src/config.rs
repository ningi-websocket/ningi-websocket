use std::fmt::{Display, Formatter};
use std::net::IpAddr;

use serde::Deserialize;

use crate::app_jwt::JwtAlgo;

pub enum ConfigParsingError {
    Io(std::io::Error),
    Parse(toml::de::Error),
}

#[derive(Deserialize, Clone)]
pub struct AppConfig {
    /// Настройки приложения
    pub daemon: DaemonConfig,
    /// Настройки "фронтенда"
    pub web: WebConfig,
    /// Настройки статистики
    pub statistic: StatisticConfig,
}

#[derive(Deserialize, Clone)]
pub struct DaemonConfig {
    /// IP, на котором висит демон
    pub ip: IpAddr,
    /// Порт, на котором висит демон
    pub port: u16,
    /// Путь к публичному ключу, которым проверяются JWT токены
    pub jwt_public_key: String,
    /// Путь к публичному ключу, которым проверяются JWT токены
    pub jwt_algo: JwtAlgo,
    /// Ключ для отправки сообщений на роут /publish
    pub publish_key: String,
    /// Секрет для подключения к "ws/broadcast". Должен быть отправлен в первом сообщении!
    pub broadcast_token: String,
    /// Путь к директории, в которую будет писаться лог.
    pub log_location: Option<String>,
    /// Если указано, будут разрешены лишь указанные топики
    pub topics: Option<Vec<String>>,
    /// Real ip header (for example: "x-real-ip")
    pub ip_header: Option<String>,
}

#[derive(Deserialize, Clone)]
pub struct WebConfig {
    /// Хост, на котором предполагается запуск демона (например "ws.example.org" или "localhost")
    pub ws_host: String,
    /// Использовать режим wss://, иначе ws://
    pub wss: bool,
    /// Реальный порт, с которым работают клиенты
    pub port: Option<u16>,
    /// Время в секундах, сколько ждать подключения клиента
    /// Если указан 0, клиенты не смогут подключиться
    pub request_timeout: u8,
    /// Время в секундах, после которого клиент будет принудительно отключен от сервера, если от него не поступало ping сообщений
    /// Служит для того, чтобы в системе не оставались "мертвые" соединения (например когда у клиента пропадает интернет и он не может корректно закрыть соединение)
    /// Не рекомендуется ставить слишком низкое/высокое значение (минимальное значение 30, рекомендуемое 30-90)
    pub max_idle_time: u16,
}

#[derive(Deserialize, Clone)]
pub struct StatisticConfig {
    /// Ключ для получения данных из статистических методов (/clients, /stat)
    pub secret: Option<String>,
}

impl Display for ConfigParsingError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigParsingError::Io(e) => {
                write!(f, "IO error: {}", e.to_string())
            }
            ConfigParsingError::Parse(e) => {
                write!(f, "Parsing error: {}", e.to_string())
            }
        }
    }
}

impl AppConfig {
    pub fn from_file(path: &std::path::Path) -> Result<AppConfig, ConfigParsingError> {
        match std::fs::read_to_string(path) {
            Ok(content) => match toml::from_str(&*content) {
                Ok(o) => Ok(o),
                Err(e) => Err(ConfigParsingError::Parse(e)),
            },
            Err(e) => Err(ConfigParsingError::Io(e)),
        }
    }
}
