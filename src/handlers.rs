use array_tool::vec::Intersect;
use chrono::{Duration, Utc};
use serde_json::json;
use uuid::Uuid;
use warp::http::{Response, StatusCode};
use warp::reply::{json, with_status};
use warp::{Rejection, Reply};

use crate::app_jwt::AppJwt;
use crate::broadcast_client::BroadcastClients;
use crate::config::AppConfig;
use crate::requests::{ClientRegisterRequest, PublishMessage, PublishRequest};
use crate::responses::*;
use crate::websocket::client::ws_client::*;

pub type WsResult<T> = Result<T, Rejection>;

pub async fn root_handler(clients: WsClients) -> WsResult<impl Reply> {
    // todo: REMOVE THIS!
    Ok(with_status(
        json(&clients.get_connections_info().await),
        StatusCode::OK,
    ))
}

pub async fn register_handler(
    body: ClientRegisterRequest,
    cfg: AppConfig,
    mut clients: WsClients,
    app_jwt: AppJwt,
) -> WsResult<impl Reply> {
    //body.token

    let claims = match app_jwt.decode(body.token.clone()) {
        Ok(o) => o.claims,
        Err(e) => {
            return Ok(with_status(
                json(&ErrorResponse {
                    reason: ErrorReason::BadToken,
                    message: format!("{e}"),
                }),
                StatusCode::BAD_REQUEST,
            ))
        }
    };

    if claims.exp <= Utc::now().timestamp() {
        return Ok(with_status(
            json(&ErrorResponse {
                reason: ErrorReason::ExpiredToken,
                message: "Expired token".to_string(),
            }),
            StatusCode::BAD_REQUEST,
        ));
    }

    let expired_at = Utc::now() + Duration::seconds(cfg.web.request_timeout as i64);

    let client = WsClient {
        user_id: claims.user_id.clone(),
        sender: None,
        last_activity: Utc::now(),
        topics: body.topics.clone(),
        connected_at: None,
        expired_at,
    };

    let connection_id = Uuid::new_v4();

    clients.add_client(connection_id, client).await;

    let schema = if cfg.web.wss { "wss" } else { "ws" };
    let host = match cfg.web.port {
        Some(p) => format!("{}:{}", cfg.web.ws_host, p),
        _ => cfg.web.ws_host,
    };

    let resp = RegisterResponse {
        expired_at,
        url: format!("{}://{}/ws/{}", schema, host, connection_id),
        topics: Some(match cfg.daemon.topics {
            Some(t) => t.intersect(body.topics),
            _ => body.topics,
        }),
    };

    Ok(with_status(json(&resp), StatusCode::CREATED))
}

pub async fn ws_handler(
    connection_id: Uuid,
    ws: warp::ws::Ws,
    cfg: AppConfig,
    clients: WsClients,
) -> WsResult<impl Reply> {
    let client = clients.find_client(connection_id.clone()).await.clone();

    match client {
        None => {
            log::info!("Ws connection {} not found", connection_id);
            let resp = ErrorResponse {
                reason: ErrorReason::ConnectionNotFound,
                message: "Connection not found".to_string(),
            };
            let resp = Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(json!(resp).to_string());
            Ok(Reply::into_response(resp))
        }
        Some(client) if client.expired_at.clone() < Utc::now() => {
            log::info!("Ws connection {} expired", connection_id);
            let resp = ErrorResponse {
                reason: ErrorReason::ExpiredConnection,
                message: "Expired connection".to_string(),
            };
            let resp = Response::builder()
                .status(StatusCode::GONE)
                .body(json!(resp).to_string());
            Ok(Reply::into_response(resp))
        }
        Some(client) if client.connected_at.clone().is_some() => {
            log::info!("Ws connection {} already connected", connection_id);
            let resp = ErrorResponse {
                reason: ErrorReason::AlreadyConnected,
                message: "Already connected".to_string(),
            };
            let resp = Response::builder()
                .status(StatusCode::FORBIDDEN)
                .body(json!(resp).to_string());
            Ok(Reply::into_response(resp))
        }
        Some(client) => {
            log::info!(
                "Handle new ws client {}. ConnectionUrlId: {}",
                client.user_id.clone(),
                connection_id.clone().to_string(),
            );

            let response = ws.on_upgrade(move |socket| {
                crate::websocket::client_connection(socket, cfg, connection_id, clients)
            });
            Ok(Reply::into_response(response))
        }
    }
}

pub async fn publish_handler(
    body: PublishRequest,
    cfg: AppConfig,
    mut clients: WsClients,
) -> WsResult<impl Reply> {
    if body.key != cfg.daemon.publish_key {
        return Ok(with_status(
            json(&PublishResponse {
                status: PublishResponseStatusEnum::AccessDenied,
            }),
            StatusCode::FORBIDDEN,
        ));
    }

    match body.message {
        PublishMessage::Bytes(_) => Ok(with_status(
            json(&PublishResponse {
                status: PublishResponseStatusEnum::Unsupported,
            }),
            StatusCode::BAD_REQUEST,
        )),
        _ => {
            clients
                .send_message(body.topic, body.message, |c| {
                    body.clients.len() == 0 || body.clients.contains(&c.user_id)
                })
                .await;

            Ok(with_status(
                json(&PublishResponse {
                    status: PublishResponseStatusEnum::Ok,
                }),
                StatusCode::OK,
            ))
        }
    }
}

pub async fn ws_broadcast_handler(
    ws: warp::ws::Ws,
    cfg: AppConfig,
    clients: WsClients,
    broadcast_clients: BroadcastClients,
) -> WsResult<impl Reply> {
    let response = ws.on_upgrade(move |socket| {
        crate::websocket::broadcast_connection(socket, cfg, clients, broadcast_clients)
    });

    Ok(Reply::into_response(response))
}
