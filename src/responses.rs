use chrono::{DateTime, Utc};
use serde::Serialize;

#[derive(Serialize)]
pub enum ErrorReason {
    BadToken,
    ExpiredToken,
    ConnectionNotFound,
    ExpiredConnection,
    AlreadyConnected,
}

#[derive(Serialize)]
pub enum PublishResponseStatusEnum {
    Ok,
    AccessDenied,
    Unsupported,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterResponse {
    /// Адрес для подключения к websocket
    pub url: String,
    /// Дата "протухания" ссылки
    pub expired_at: DateTime<Utc>,
    /// Список доступных топиков при подключении
    #[serde(skip_serializing_if = "Option::is_none")]
    pub topics: Option<Vec<String>>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ErrorResponse {
    /// Причина отказа
    pub reason: ErrorReason,
    /// Причина отказа текстом
    pub message: String,
}

#[derive(Serialize)]
pub struct PublishResponse {
    pub status: PublishResponseStatusEnum,
}
