use std::time::Duration;
use chrono::Utc;
use uuid::Uuid;
use warp::ws::Message;
use crate::broadcast_client::BroadcastClients;
use crate::websocket::client::ws_client::{WsClient, WsClientsUsages};
use crate::websocket::client::ws_client::WsClients;

pub trait WsClientBg {
    fn ping(&self, dur: Duration) -> impl std::future::Future<Output = ()> + Send;

    fn clear_dead(&self, dur: Duration) -> impl std::future::Future<Output = ()> + Send;
}

impl WsClientBg for WsClients {
    async fn ping(&self, dur: Duration) {
        loop {
            tokio::time::sleep(Duration::from_secs(30)).await;
            log::debug!("Send ping message");

            let msg = Message::text("ping".to_string());

            for (id, client) in self.active_clients(dur).await {
                if let Err(e) = client.sender.unwrap().send(Ok(msg.clone())) {
                    log::debug!("Cannot send message to client {id}: {e}");
                }
            }
        }
    }

    async fn clear_dead(&self, dur: Duration) {
        let t = Utc::now();

        log::debug!("Start removing dead connections");

        let removed_clients: Vec<(Uuid, WsClient)> = self
            .clients
            .read()
            .await
            .iter()
            .clone()
            .filter(|(_, c)| (c.last_activity.clone() + dur) < t)
            .map(|(id, u)| (id.clone(), u.clone()))
            .collect();

        log::debug!(
            "Filter removed {} clients: {}",
            removed_clients.len(),
            Utc::now() - t
        );

        for (k, client) in removed_clients.iter() {
            if let Some(c) = client.sender.clone() {
                if let Err(e) = c.send(Ok(Message::close())) {
                    log::debug!("Cannot send close frame: {}", e);
                }
            }
            self.clients.write().await.remove(k);
        }

        log::debug!("Filter summary: {}", Utc::now() - t);
    }
}

impl WsClientBg for BroadcastClients {
    async fn ping(&self, _dur: Duration) {
        loop {
            tokio::time::sleep(Duration::from_secs(30)).await;
            log::debug!("Send ping message");

            let msg = Message::text("ping".to_string());

            for (id, client) in self.clients.read()
                .await
                .iter()
                .filter(|(_, c)| c.authorized) {
                if let Err(e) = client.sender.send(Ok(msg.clone())) {
                    log::debug!("Cannot send message to broadcast client {id}: {e}");
                }
            }
        }
    }

    async fn clear_dead(&self, _dur: Duration) {
        loop {
            // todo: Maybe not need?
            tokio::time::sleep(Duration::from_secs(1)).await;
        }
    }
}