use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::json;
use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::RwLock;
use uuid::Uuid;
use warp::ws::Message;

use crate::requests::PublishMessage;

pub type WsSender = UnboundedSender<Result<Message, warp::Error>>;

#[derive(Clone)]
pub struct WsClient {
    pub user_id: Uuid,
    pub sender: Option<WsSender>,
    pub last_activity: DateTime<Utc>,
    pub topics: Vec<String>,
    pub connected_at: Option<DateTime<Utc>>,
    pub expired_at: DateTime<Utc>,
}

#[derive(Serialize)]
//#[serde(untagged)]
pub enum WsMessageType {
    #[serde(rename = "ok")]
    Ok,
    #[serde(rename = "topics")]
    Topics,
    #[serde(rename = "error")]
    Error,
    #[serde(rename = "OFFLINE-MESSAGES-SYSTEM")]
    Offline,
    #[serde(rename = "USER-TO-USER")]
    UserToUser,
}

#[deprecated]
#[derive(Serialize, Debug)]
pub struct ConnectionInfo {
    id: Uuid,
    connected: bool,
    la: DateTime<Utc>,
}

// #[serde(rename_all = "camelCase")]
#[derive(Serialize)]
pub struct WebSocketMessage {
    pub r#type: WsMessageType,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<Uuid>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub topic: Option<String>,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub content: String,
    #[serde(rename = "b", skip_serializing_if = "std::ops::Not::not")]
    pub broadcast: bool,
}

#[derive(Deserialize, Clone)]
pub struct BroadcastMessage {
    pub clients: Vec<Uuid>,
    pub content: String,
    pub topic: String,
}

#[derive(Deserialize)]
pub struct BroadcastAuthorizeMessage {
    pub token: String,
}

type ClientsType = Arc<RwLock<HashMap<Uuid, WsClient>>>;

#[derive(Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct WsClients {
    /// {connection_id: <client>}
    #[serde(skip_serializing)]
    pub(super) clients: ClientsType,
    messages_in: usize,
    messages_out: usize,
    messages_out_err: usize,
    client_messages_in: usize,
    client_messages_out: usize,
    client_messages_out_err: usize,
    max_clients: usize,
    max_connections: usize,
    user_to_user: HashMap<Uuid, usize>, // <senderId, <receiverId, count>>
}

impl WebSocketMessage {
    fn with_type(t: WsMessageType, content: String) -> WebSocketMessage {
        WebSocketMessage {
            topic: None,
            from: None,
            content,
            r#type: t,
            broadcast: false,
        }
    }

    pub fn ok() -> WebSocketMessage {
        Self::with_type(WsMessageType::Ok, "".to_string())
    }

    pub fn err(msg: String) -> WebSocketMessage {
        Self::with_type(WsMessageType::Error, msg)
    }
}

impl WsClients {
    #[deprecated]
    pub async fn get_connections_info(&self) -> Vec<ConnectionInfo> {
        self.clients
            .read()
            .await
            .iter()
            .map(|(id, conn)| ConnectionInfo {
                id: id.clone(),
                connected: conn.connected_at.is_some(),
                la: conn.last_activity,
            })
            .collect()
    }
}

pub trait WsClientsUsages {
    async fn new() -> Self;

    async fn find_client(&self, connection_id: Uuid) -> Option<WsClient>;

    async fn active_clients(&self, dur: Duration) -> Vec<(Uuid, WsClient)>;

    async fn update_activity(&mut self, connection_id: Uuid);

    async fn send_message<T>(&mut self, topic: String, message: PublishMessage, filter: T)
    where
        T: Fn(&WsClient) -> bool;

    async fn send_broadcast_message<T>(&mut self, message: BroadcastMessage, filter: T)
    where
        T: Fn(&WsClient) -> bool;

    async fn send_user_to_user(&mut self, from_user: Uuid, to_user: Uuid, message: Message);

    async fn set_sender(&mut self, connection_id: Uuid, sender: Option<WsSender>);

    async fn delete(&mut self, connection_id: Uuid);

    async fn add_client(&mut self, connection_id: Uuid, ws_client: WsClient);
}

impl WsClientsUsages for WsClients {
    async fn new() -> Self {
        Self {
            clients: Arc::new(RwLock::new(HashMap::new())),
            messages_in: 0,
            messages_out: 0,
            messages_out_err: 0,
            client_messages_in: 0,
            client_messages_out: 0,
            client_messages_out_err: 0,
            max_clients: 0,
            max_connections: 0,
            user_to_user: HashMap::new(),
        }
    }

    async fn find_client(&self, connection_id: Uuid) -> Option<WsClient> {
        self.clients
            .read()
            .await
            .get(&connection_id.clone())
            .cloned()
    }

    async fn active_clients(&self, dur: Duration) -> Vec<(Uuid, WsClient)> {
        let time_now = Utc::now();
        self.clients
            .read()
            .await
            .iter()
            .filter(|(_, c)| match c.sender.clone() {
                Some(s) => {
                    log::debug!(
                        "Client: {} // last_activity: {} // is_closed: {:?}",
                        c.user_id,
                        c.last_activity,
                        s.is_closed()
                    );
                    c.connected_at.is_some()
                        && (c.last_activity.clone() + dur) > time_now
                        && !s.is_closed()
                }
                None => {
                    log::info!("No sender found for {}!", c.user_id);
                    false
                }
            })
            .map(|(id, u)| (id.clone(), u.clone()))
            .collect()
    }

    async fn update_activity(&mut self, connection_id: Uuid) {
        if let Some(c) = self.clients.write().await.get_mut(&connection_id) {
            log::debug!("Update activity for {}", connection_id.clone());

            c.last_activity = Utc::now();
        }
    }

    async fn send_message<T>(&mut self, topic: String, message: PublishMessage, filter: T)
    where
        T: Fn(&WsClient) -> bool,
    {
        self.messages_in += 1;

        let msg = match message {
            PublishMessage::Bytes(content) => Message::binary(content),
            PublishMessage::String(content) => Message::text(
                json!(&WebSocketMessage {
                    r#type: WsMessageType::Topics,
                    topic: Some(topic.clone()),
                    from: None,
                    content,
                    broadcast: false,
                })
                .to_string(),
            ),
        };

        let (messages_out, messages_out_err) =
            send_message(self.clients.clone(), topic.clone(), msg, filter).await;

        self.messages_out += messages_out;
        self.messages_out_err += messages_out_err;
    }

    async fn send_broadcast_message<T>(&mut self, message: BroadcastMessage, filter: T)
    where
        T: Fn(&WsClient) -> bool,
    {
        self.messages_in += 1;

        let msg = Message::text(
            json!(&WebSocketMessage {
                r#type: WsMessageType::Topics,
                topic: Some(message.topic.clone()),
                from: None,
                content: message.content,
                broadcast: true,
            })
            .to_string(),
        );

        let (messages_out, messages_out_err) =
            send_message(self.clients.clone(), message.topic.clone(), msg, filter).await;

        self.messages_out += messages_out;
        self.messages_out_err += messages_out_err;
    }

    async fn send_user_to_user(&mut self, from_user: Uuid, to_user: Uuid, message: Message) {
        self.client_messages_in += 1;
        if !message.is_text() {
            return;
        }
        let content = match message.to_str() {
            Ok(s) => s.to_string(),
            Err(_) => {
                log::warn!("Unexpected user2user message");
                return;
            }
        };
        let msg = Message::text(
            json!(&WebSocketMessage {
                r#type: WsMessageType::UserToUser,
                topic: None,
                from: Some(from_user),
                content,
                broadcast: false,
            })
            .to_string(),
        );

        if self.user_to_user.get(&from_user).is_none() {
            self.user_to_user.insert(from_user, 0);
        }
        *self.user_to_user.get_mut(&from_user).unwrap() += 1;

        self.clients
            .read()
            .await
            .iter()
            .filter(|(_, c)| c.user_id.clone() == to_user.clone() && c.connected_at.is_some())
            .for_each(|(_, c)| {
                match c.sender.clone() {
                    Some(s) if !s.is_closed() => match s.send(Ok(msg.clone())) {
                        Ok(_) => {
                            self.client_messages_out += 1;
                        }
                        Err(e) => {
                            log::warn!("Cannot send user2user message: {e}");
                            self.client_messages_out_err += 1;
                        }
                    },
                    _ => {}
                };
            });
    }

    async fn set_sender(&mut self, connection_id: Uuid, sender: Option<WsSender>) {
        log::info!("Set sender for {}", connection_id);

        let current_connections = self.clients.read().await.len();
        if self.max_connections < current_connections {
            self.max_connections = current_connections + 1;
        }

        if let Some(client) = self.clients.write().await.get_mut(&connection_id) {
            client.sender = sender;
            client.connected_at = Some(Utc::now());
            client.last_activity = Utc::now();
        }
    }

    async fn delete(&mut self, connection_id: Uuid) {
        self.clients.write().await.remove(&connection_id);
    }

    async fn add_client(&mut self, connection_id: Uuid, ws_client: WsClient) {
        self.clients.write().await.insert(connection_id, ws_client);

        let current_clients = self.clients.read().await.iter().filter(|(_, c)| c.sender.is_some()).count();
        if self.max_clients <= current_clients {
            self.max_clients = current_clients + 1;
        }
    }
}

async fn send_message<T>(
    clients: ClientsType,
    topic: String,
    message: Message,
    filter: T,
) -> (usize, usize)
where
    T: Fn(&WsClient) -> bool,
{
    log::debug!("New message with topic: {topic}");
    let clients = Vec::from(
        clients
            .read()
            .await
            .iter()
            .filter(|(_, c)| c.connected_at.is_some() && c.topics.contains(&topic.clone()))
            .filter(|(_, c)| filter(c))
            .map(|(_, c)| c.clone())
            .collect::<Vec<WsClient>>(),
    );
    log::debug!(r#"Found: {} clients with topic "{topic}""#, clients.len());

    let (mut messages_out, mut messages_out_err) = (0, 0);

    for client in clients {
        if let Some(s) = client.sender {
            if s.is_closed() {
                continue;
            }

            if let Err(e) = s.send(Ok(message.clone())) {
                messages_out_err += 1;
                log::warn!("Cannot send message to {}: {e}", client.user_id.clone());
            } else {
                messages_out += 1;
            }
        }
    }

    (messages_out, messages_out_err)
}
