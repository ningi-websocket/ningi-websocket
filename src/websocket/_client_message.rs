use array_tool::vec::Intersect;
use serde::Deserialize;
use serde_json::{from_str, json};
use tokio::sync::mpsc::UnboundedSender;
use uuid::Uuid;
use warp::ws::Message;

use crate::config::WebConfig;
use crate::websocket::client::ws_client::{WebSocketMessage, WsClient, WsClients, WsClientsUsages};

type Sender = UnboundedSender<Result<Message, warp::Error>>;

#[derive(Deserialize)]
struct MessageUpdateTopics {
    topics: Vec<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct MessageUserToUser {
    to_user: Uuid,
    message: String,
}

#[derive(Clone, Debug)]
pub enum ClientMessage {
    Pong,
    PongJs,
    Ping,
    PingJs,
    UpdateTopics,
    UserToUser,
}

#[derive(Clone, Debug)]
pub enum ClientMessageError {
    ClientSendPongError,
    ClientBadTextMessageError,
    ClientUnexpectedTextMessage,
    UndefinedMessage,
    NoSenderFound,
    NoClientFound,
    TopicResponseError,
}

pub async fn client_message(
    connection_id: Uuid,
    msg: Message,
    mut clients: WsClients,
    allowed_topics: Option<Vec<String>>,
    _cfg: WebConfig,
    // user_events_counter: utils::counters_util::WsUserEventsCounter,
    // clients_events: ClientsEvents,
) -> Result<ClientMessage, ClientMessageError> {
    log::debug!("Received message from {}: {:?}", connection_id, msg);

    clients.update_activity(connection_id).await;
    let client = match clients.find_client(connection_id).await {
        Some(c) => c,
        _ => return Err(ClientMessageError::NoClientFound),
    };

    if msg.is_pong() {
        log::debug!("Client PONG message");
        return Ok(ClientMessage::Pong);
    }

    if msg.is_ping() {
        log::debug!("Client PING message");
        return match send_msg(
            Message::pong(msg.as_bytes()),
            client.sender.unwrap().clone(),
            connection_id.clone(),
        )
        .await
        {
            Ok(_) => Ok(ClientMessage::Ping),
            Err(_) => Err(ClientMessageError::ClientSendPongError),
        };
    };

    if msg.is_text() {
        return match msg.to_str().map(|m| m.to_string()) {
            Ok(message) => {
                if let Some(c) = client.sender.clone() {
                    return client_message_text(
                        client.clone(),
                        c.clone(),
                        message,
                        connection_id.clone(),
                        allowed_topics.clone(),
                        clients.clone(),
                    )
                    .await;
                }
                Err(ClientMessageError::NoSenderFound)
            }
            Err(_) => {
                log::warn!("Cannot get text from text message!");
                Err(ClientMessageError::ClientBadTextMessageError)
            }
        };
    };

    Err(ClientMessageError::UndefinedMessage)
}

async fn send_msg(msg: Message, sender: Sender, connection_id: Uuid) -> Result<(), ()> {
    match sender.clone().send(Ok(msg)) {
        Ok(_) => Ok(()),
        Err(e) => {
            log::info!(
                "Error when sending message to client {}: {}",
                connection_id.clone(),
                e
            );

            Err(())
        }
    }
}

async fn client_message_text(
    client: WsClient,
    sender: Sender,
    msg: String,
    connection_id: Uuid,
    allowed_topics: Option<Vec<String>>,
    clients: WsClients,
) -> Result<ClientMessage, ClientMessageError> {
    if msg.clone() == "ping".to_string() {
        log::debug!(r#"Client "PING" message"#);
        return match send_msg(Message::text("pong"), sender.clone(), connection_id.clone()).await {
            Ok(_) => Ok(ClientMessage::PingJs),
            Err(_) => Err(ClientMessageError::ClientSendPongError),
        };
    };
    if msg.clone() == "pong".to_string() {
        log::debug!(r#"Client "PONG" message: {connection_id}"#);
        return Ok(ClientMessage::PongJs);
    };

    match try_update_topics(client.clone(), msg.clone(), allowed_topics).await {
        Ok(_t) => {
            log::debug!(r#"Client "TOPICS" updated {connection_id}"#);
            if let Err(e) = sender.send(Ok(Message::text(
                json!(&WebSocketMessage::ok()).to_string(),
            ))) {
                log::debug!("Cannot reply to client: {connection_id} {e}");
                return Err(ClientMessageError::TopicResponseError);
            };
            return Ok(ClientMessage::UpdateTopics);
        }
        Err(_) => {}
    };

    // отправляем сообщение другим пользователям
    match try_send_user_to_user_message(client.clone(), msg.clone(), clients.clone()).await {
        Ok(_) => {
            log::debug!("Client u2u message: {connection_id}");
            return Ok(ClientMessage::UserToUser);
        }
        Err(_) => {}
    };

    log::debug!("Bad message {msg:?} from {connection_id}");
    if let Err(e) = sender.send(Ok(Message::text(
        json!(&WebSocketMessage::err("Bad message".to_string())).to_string(),
    ))) {
        log::debug!("Cannot reply to client: {connection_id} {e}");
        return Err(ClientMessageError::TopicResponseError);
    };

    Err(ClientMessageError::ClientUnexpectedTextMessage)
}

async fn try_update_topics(
    mut client: WsClient,
    message: String,
    allowed_topics: Option<Vec<String>>,
) -> Result<Vec<String>, ()> {
    match from_str::<MessageUpdateTopics>(message.as_str()) {
        Ok(msg) => {
            let topics = match allowed_topics {
                Some(t) => msg.topics.intersect(t),
                None => msg.topics,
            }
            .iter()
            .map(|t| t.to_lowercase())
            .collect::<Vec<String>>();

            client.topics = topics.clone();
            Ok(topics)
        }
        _ => Err(()),
    }
}

async fn try_send_user_to_user_message(
    client: WsClient,
    message: String,
    mut clients: WsClients,
) -> Result<(), ()> {
    match from_str::<MessageUserToUser>(message.as_str()) {
        Ok(msg) => {
            clients
                .send_user_to_user(
                    client.user_id,
                    msg.to_user.clone(),
                    Message::text(msg.message),
                )
                .await;
            Ok(())
        }
        _ => Err(()),
    }
}
