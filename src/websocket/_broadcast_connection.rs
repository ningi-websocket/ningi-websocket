use chrono::Utc;
use futures_util::StreamExt;
use serde_json::from_str;
use serde_json::json;
use tokio::sync::mpsc::UnboundedSender;
use uuid::Uuid;
use warp::ws::{Message, WebSocket};

use crate::broadcast_client::{BroadcastClient, BroadcastClients, BroadcastClientsUsages};
use crate::config::{AppConfig, DaemonConfig};
use crate::websocket::SenderType;
use crate::websocket::client::ws_client::{
    BroadcastAuthorizeMessage, BroadcastMessage, WebSocketMessage, WsClients, WsClientsUsages,
};

type Sender = UnboundedSender<Result<Message, warp::Error>>;

#[derive(Debug)]
pub enum BroadcastMessageEnum {
    Ping,
    Pong,
    Text,
    Binary,
}

#[derive(Debug)]
pub enum BroadcastMessageErrorEnum {
    CannotReply,
    CannotGetMessageText,
    MessageParsingError,
    Undefined,
}

pub async fn broadcast_connection(
    ws: WebSocket,
    cfg: AppConfig,
    clients: WsClients,
    mut broadcast_clients: BroadcastClients,
    // TODO: выводить инфу о ip клиента.
) {
    let (mut client_ws_rcv, client_sender) = super::spawn_pipe(ws).await;

    let connection_id = Uuid::new_v4();
    let client = BroadcastClient {
        id: connection_id,
        connected_at: Utc::now(),
        sender: client_sender.clone(),
        authorized: false,
    };

    broadcast_clients.add_client(client).await;

    while let Some(result) = client_ws_rcv.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                log::error!(r#"Error receiving message from "{connection_id}": {e}"#);
                break;
            }
        };

        if !broadcast_clients.is_authorized(connection_id.clone()).await {
            log::info!("Broadcast client {connection_id} not authorized");
            if client_authorization_attempt(
                connection_id.clone(),
                msg,
                cfg.daemon.clone(),
                client_sender.clone(),
            )
            .await
            {
                broadcast_clients.authorize(connection_id.clone()).await;
                if let Err(e) = client_sender.send(Ok(Message::text("Authorized"))) {
                    log::info!("Cannot send message to broadcast client {connection_id}: {e}");
                }
                continue;
            }
            break;
        }

        broadcast_clients.inc_in().await;

        if let Err(e) = broadcast_message(
            connection_id.clone(),
            msg,
            client_sender.clone(),
            cfg.clone(),
            clients.clone(),
        )
        .await
        {
            log::error!("Error: {:?}", e);
            break;
        }
    }

    log::error!("Connection {} deleted", connection_id);
    broadcast_clients.delete(connection_id).await;
}

async fn broadcast_message(
    connection_id: Uuid,
    msg: Message,
    sender: Sender,
    cfg: AppConfig,
    clients: WsClients,
) -> Result<BroadcastMessageEnum, BroadcastMessageErrorEnum> {
    if msg.is_pong() {
        log::debug!(r#"Broadcast: "PING" message"#);
        return Ok(BroadcastMessageEnum::Pong);
    }

    if msg.is_ping() {
        log::debug!(r#"Broadcast: "PONG" message"#);
        if let Err(e) = sender.send(Ok(Message::pong(msg.as_bytes()))) {
            log::warn!("Cannot reply to broadcast: {}", e);
            return Err(BroadcastMessageErrorEnum::CannotReply);
        }

        return Ok(BroadcastMessageEnum::Ping);
    }

    if msg.is_binary() {
        let text_msg = json!(&WebSocketMessage::err("Not supported now".to_string())).to_string();

        if let Err(e) = sender.send(Ok(Message::text(text_msg))) {
            log::warn!("Cannot reply to broadcast: {}", e);
            return Err(BroadcastMessageErrorEnum::CannotReply);
        }

        return Ok(BroadcastMessageEnum::Binary);
    }

    if msg.is_text() {
        return match msg.to_str().map(|m| m.to_string()) {
            Ok(message) => {
                match message_text(
                    message,
                    connection_id.clone(),
                    sender.clone(),
                    cfg.clone(),
                    clients.clone(),
                )
                .await
                {
                    Ok(o) => Ok(o),
                    Err(e) => Err(e),
                }
            }
            Err(_) => {
                log::warn!("Cannot get text from message: {connection_id}");
                Err(BroadcastMessageErrorEnum::CannotGetMessageText)
            }
        };
    }

    log::warn!("Undefined message type");
    return Err(BroadcastMessageErrorEnum::Undefined);
}

async fn message_text(
    message: String,
    connection_id: Uuid,
    sender: Sender,
    cfg: AppConfig,
    mut clients: WsClients,
) -> Result<BroadcastMessageEnum, BroadcastMessageErrorEnum> {
    return match from_str::<BroadcastMessage>(message.as_str()) {
        Ok(body) => {
            match cfg.daemon.topics {
                Some(t) if !t.contains(&body.topic.clone()) => {
                    let text_msg =
                        json!(&WebSocketMessage::err("Topic not allowed".to_string())).to_string();
                    if let Err(e) = sender.send(Ok(Message::text(text_msg))) {
                        log::warn!("Cannot reply to broadcast (received): {connection_id} {e}");
                        return Err(BroadcastMessageErrorEnum::CannotReply);
                    }
                }
                _ => {
                    clients
                        .send_broadcast_message(body.clone(), |c| {
                            body.clients.len() == 0 || body.clients.contains(&c.user_id.clone())
                        })
                        .await;
                }
            }

            if let Err(e) = sender.send(Ok(Message::text(
                json!(&WebSocketMessage::ok()).to_string(),
            ))) {
                log::warn!("Cannot reply to broadcast: {connection_id} {e}");
            }

            Ok(BroadcastMessageEnum::Text)
        }
        Err(e) => {
            log::warn!("Message parsing error: {connection_id} {e}");

            Err(BroadcastMessageErrorEnum::MessageParsingError)
        }
    };
}

async fn client_authorization_attempt(
    connection_id: Uuid,
    msg: Message,
    cfg: DaemonConfig,
    client_sender: SenderType,
) -> bool {
    if msg.is_text() {
        match msg
            .to_str()
            .map(|m| match from_str::<BroadcastAuthorizeMessage>(m) {
                Ok(t) => t.token,
                Err(_) => {
                    log::info!("Bad authorization message: {m}");
                    "".to_string()
                }
            }) {
            Ok(t) if t == cfg.broadcast_token.clone() => {
                log::info!("Success authorized broadcast client {connection_id}");
                return true;
            }
            Ok(t) => {
                log::info!(r#"Received invalid broadcast token: "{t}""#)
            }
            _ => {}
        }
    }

    log::warn!("Failed authorized broadcast client {connection_id}");

    if let Err(e) = client_sender.send(Ok(Message::close_with(1u16, "Unauthorized"))) {
        // because we can ignore it.
        log::debug!("Cannot send message to broadcast client {connection_id}: {e}");
    }

    false
}
