use futures_util::StreamExt;
use uuid::Uuid;
use warp::ws::WebSocket;

use crate::config::AppConfig;
use crate::websocket::client::ws_client::{WsClients, WsClientsUsages};

pub async fn client_connection(
    ws: WebSocket,
    cfg: AppConfig,
    connection_id: Uuid,
    mut clients: WsClients,
) {
    let allowed_topics = cfg.daemon.topics.clone();

    let (mut client_ws_rcv, client_sender) = super::spawn_pipe(ws).await;

    clients.set_sender(connection_id, Some(client_sender)).await;

    // todo: check client connections limit

    while let Some(result) = client_ws_rcv.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                log::error!(r#"Error receiving ws message from "{connection_id}": {e}"#);
                break;
            }
        };

        match super::client_message(
            connection_id.clone(),
            msg.clone(),
            clients.clone(),
            allowed_topics.clone(),
            cfg.web.clone(),
        )
        .await
        {
            Ok(_m) => {}
            Err(e) => {
                log::warn!("Client message error: {:?}", e);
                break;
            }
        }
    }

    log::info!("Client connection {} deleted", connection_id);
    clients.delete(connection_id).await;
}
