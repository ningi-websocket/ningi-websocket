use futures_util::stream::SplitStream;
use futures_util::FutureExt;
use futures_util::StreamExt;
use tokio::sync::mpsc;
use tokio::sync::mpsc::UnboundedSender;
use tokio::task;
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::WebSocket;

pub use _broadcast_connection::broadcast_connection;
pub use _client_connection::client_connection;
pub(self) use _client_message::*;

mod _broadcast_connection;
mod _client_connection;
mod _client_message;
pub mod client;

type SenderType = UnboundedSender<Result<warp::ws::Message, warp::Error>>;

pub async fn spawn_pipe(ws: WebSocket) -> (SplitStream<WebSocket>, SenderType) {
    let (client_ws_sender, client_ws_rcv) = ws.split();
    let (client_sender, client_rcv) = mpsc::unbounded_channel();

    let stream = UnboundedReceiverStream::new(client_rcv);

    task::spawn(stream.forward(client_ws_sender).map(|result| {
        if let Err(e) = result {
            log::warn!("Pipe error: {}", e)
        }
    }));

    (client_ws_rcv, client_sender)
}
