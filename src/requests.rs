use serde::Deserialize;
use uuid::Uuid;

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum PublishMessage {
    String(String),
    Bytes(Vec<u8>),
}

#[derive(Deserialize, Debug)]
pub struct PublishRequest {
    pub topic: String,
    #[serde(default)]
    pub clients: Vec<Uuid>,
    pub message: PublishMessage,
    pub key: String,
}

#[derive(Deserialize, Debug)]
pub struct ClientRegisterRequest {
    pub token: String,
    #[serde(default)]
    pub topics: Vec<String>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Claims {
    /// User identifier
    pub user_id: uuid::Uuid,
    /// Expiration time (as UTC timestamp)
    pub exp: i64,
}

// "default" fields
/*
struct Claims {
    aud: String,         // Optional. Audience
    exp: usize,          // Required (validate_exp defaults to true in validation). Expiration time (as UTC timestamp)
    iat: usize,          // Optional. Issued at (as UTC timestamp)
    iss: String,         // Optional. Issuer
    nbf: usize,          // Optional. Not Before (as UTC timestamp)
    sub: String,         // Optional. Subject (whom token refers to)
}
*/
