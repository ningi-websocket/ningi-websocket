use std::collections::HashMap;
use std::sync::Arc;

use chrono::{DateTime, Utc};
use tokio::sync::RwLock;
use uuid::Uuid;

use crate::websocket::client::ws_client::WsSender;

pub struct BroadcastClient {
    pub id: Uuid,
    pub sender: WsSender,
    pub connected_at: DateTime<Utc>,
    pub authorized: bool,
}

#[derive(Clone)]
pub struct BroadcastClients {
    pub clients: Arc<RwLock<HashMap<Uuid, BroadcastClient>>>,
    pub messages_in: usize,
    // messages_out: usize,
}

pub trait BroadcastClientsUsages<T> {
    async fn new() -> T;

    async fn add_client(&mut self, client: BroadcastClient);

    async fn delete(&mut self, connection_id: Uuid);

    async fn inc_in(&mut self);

    // async fn inc_out(&mut self);

    async fn is_authorized(&self, connection_id: Uuid) -> bool;

    async fn authorize(&mut self, connection_id: Uuid);
}

impl BroadcastClientsUsages<BroadcastClients> for BroadcastClients {
    async fn new() -> BroadcastClients {
        BroadcastClients {
            clients: Arc::new(RwLock::new(HashMap::new())),
            messages_in: 0,
            // messages_out: 0,
        }
    }

    async fn add_client(&mut self, client: BroadcastClient) {
        self.clients.write().await.insert(client.id, client);
    }

    async fn delete(&mut self, connection_id: Uuid) {
        self.clients.write().await.remove(&connection_id);
    }

    async fn inc_in(&mut self) {
        self.messages_in += 1;
    }

    // async fn inc_out(&mut self) {
    //     self.messages_out += 1;
    // }

    async fn is_authorized(&self, connection_id: Uuid) -> bool {
        self.clients
            .read()
            .await
            .get(&connection_id)
            .map(|c| c.authorized)
            .unwrap_or_else(|| false)
    }

    async fn authorize(&mut self, connection_id: Uuid) {
        self.clients
            .write()
            .await
            .get_mut(&connection_id)
            .map(|c| {
                c.authorized = true;
                c.connected_at = Utc::now();
            });
    }
}
