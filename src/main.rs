use std::net::SocketAddr;
use std::path::Path;
use std::time::Duration;

use crate::app_jwt::AppJwt;
use crate::broadcast_client::{BroadcastClients, BroadcastClientsUsages};
use crate::websocket::client::ws_client::{WsClients, WsClientsUsages};
use crate::websocket::client::ws_client_bg::WsClientBg;

mod app_jwt;
mod broadcast_client;
mod config;
mod handlers;
mod requests;
mod responses;
mod routes;
pub mod websocket;

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), String> {
    env_logger::init();
    log::info!("Init config");

    let cli = clap::Command::new("NingiWs")
        .arg(
            clap::Arg::new("config")
                .long("config")
                .short('c')
                .default_value("config.toml")
                .help("Path to config.toml"),
        )
        .get_matches();

    let cfg_path = Path::new(cli.get_one::<String>("config").unwrap());

    let cfg = match config::AppConfig::from_file(cfg_path) {
        Ok(o) => o,
        Err(e) => {
            log::error!("{e}");
            return Err("Config error".to_string());
        }
    };

    if cfg.web.max_idle_time < 30 {
        return Err("Max idle time cannot be less than 10!".to_string());
    }

    let app_jwt = match AppJwt::new(
        Path::new(cfg.daemon.jwt_public_key.clone().as_str()),
        cfg.daemon.jwt_algo.clone(),
    ) {
        Ok(o) => o,
        Err(e) => return Err(format!("Jwt error: {e}")),
    };

    let clients = WsClients::new().await;
    let broadcast_clients = BroadcastClients::new().await;

    let dead_client_duration = Duration::from_secs(10 + (cfg.web.max_idle_time as u64));

    tokio::spawn(bg_ping(clients.clone(), dead_client_duration.clone()));
    tokio::spawn(bg_clear_dead(clients.clone(), dead_client_duration.clone()));

    tokio::spawn(bg_ping(broadcast_clients.clone().clone(), dead_client_duration.clone()));
    tokio::spawn(bg_clear_dead(broadcast_clients.clone(), dead_client_duration.clone()));

    let addr = SocketAddr::new(cfg.daemon.ip.clone(), cfg.daemon.port.clone());

    warp::serve(
        routes::routes(
            cfg.clone(),
            clients.clone(),
            app_jwt.clone(),
            broadcast_clients.clone(),
        )
        .await,
    )
    .bind(addr)
    .await;

    Ok(())
}

async fn bg_ping<T: WsClientBg>(clients: T, dur: Duration) {
    clients.ping(dur).await
}

async fn bg_clear_dead<T: WsClientBg>(clients: T, dur: Duration) {
    clients.clear_dead(dur).await
}
